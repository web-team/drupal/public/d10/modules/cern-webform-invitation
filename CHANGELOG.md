# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.0.1] - 26/10/2023

- Updated `token` to version `^1.13`.

## [3.0.1] - 02/02/2023
- Bump dependencies to ensure compatibility.

## [3.0.0] - 02/02/2023
- Web Team assumes ownership of module.
- Update PHP 8.1 compatibility.
- Replace `isset` with `??`.
- Add CHANGELOG.md.